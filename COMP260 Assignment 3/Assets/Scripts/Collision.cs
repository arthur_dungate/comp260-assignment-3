﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class Collision : MonoBehaviour {
	public AudioClip firingClip;
	public AudioClip wallCollideClip;
	public AudioClip killClip;
	public AudioSource audioSource;

	bool active=false;
	public bool endGame;
	void Start() {
		StartCoroutine(delay());
		audioSource = GetComponent<AudioSource>();

	}


	void OnCollisionEnter2D(Collision2D collision) {
		
	
		if (active && collision.gameObject.tag == "Player") {
			collision.gameObject.GetComponent<Timer> ().endGame = true;
			collision.gameObject.GetComponent<Move> ().enabled = false;
			collision.gameObject.GetComponent<BallSpawner> ().enabled = false;
			audioSource.PlayOneShot(killClip);

		}
		else if (collision.gameObject.tag == "Wall"){
			audioSource.PlayOneShot(wallCollideClip);
			active = true;
	}
		else if (collision.gameObject.tag == "Player") {
 			audioSource.PlayOneShot(firingClip);
		}
	


		
	}


	IEnumerator delay() {
		
		yield return new WaitForSeconds(1);
		active = true;
	
	
	}

}