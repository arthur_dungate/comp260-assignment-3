﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour {

	public int numBalls = 0;
	public GameObject ballPrefab;
	public float offSetRangemax = 1.6f;
	public float offSetRangemin = 1.5f;

	// Use this for initialization
	void Update() {
		if (Input.GetKeyDown ("space"))
			SpawnBalls();
		

	}


	void SpawnBalls()
	{
		
			Vector2 spawnOffset = new Vector2 (Random.Range(-offSetRangemax, offSetRangemin), Random.Range(-offSetRangemax, offSetRangemin));
			Instantiate(ballPrefab, (Vector2)transform.position + spawnOffset, Quaternion.identity);
			numBalls += 1;

	}
}
