﻿using UnityEngine;

using System.Collections;

public class CreateSpawner : MonoBehaviour {

	public int npk = 1;
	public  GameObject pickupPrefab;
	public float xMin, yMin;
	public float width, height;

	void Start () {
		// create bees
		if (npk < 1) {
			// instantiate a bee
			GameObject pickup = Instantiate(pickupPrefab);

			// move the bee to a random position within 
			// the bounding rectangle
			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			pickup.transform.position = new Vector2(x,y);
		}
	}
}