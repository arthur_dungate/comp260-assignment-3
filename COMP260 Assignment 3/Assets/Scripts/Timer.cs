﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
	

	public float timeCounter;
	public float scoreCounter;
	public Canvas canvas;
	Text textLabel;
	Text ballLabel;
	Text scoreLabel;
	public bool endGame;


	BallSpawner ballSpawner;
	Collision collision;

	void Start(){
		ballSpawner = GetComponent<BallSpawner> ();
		Text[] textLabels=canvas.GetComponentsInChildren <Text>(true);
		for (int i = 0; i < 3; i++) {
			
			if (textLabels[i].name=="Timer"){
				textLabel=textLabels[i];
			}
			if (textLabels[i].name=="numBalls"){
				ballLabel=textLabels[i];
			}
			if (textLabels[i].name=="Score"){
				scoreLabel=textLabels[i];
			}

		}
		

	}
	void Update() {
		collision = GetComponent<Collision> ();
		if (!endGame) {
			scoreCounter += Time.deltaTime;
			timeCounter += Time.deltaTime;

			var minutes = timeCounter / 60; //Divide the guiTime by sixty to get the minutes.
			var seconds = timeCounter % 60;//Use the euclidean division for the seconds.
			var fraction = (timeCounter * 100) % 100;

			//update the label value
			textLabel.text = string.Format ("{0:00} : {1:00} : {2:000}", minutes, seconds, fraction);

			ballLabel.text = ballSpawner.numBalls + "X";
		} else {
			
		
		
			scoreLabel.text = "Score: " + ((int)(ballSpawner.numBalls * scoreCounter)).ToString();
			scoreLabel.enabled=true;
		}

		if (timeCounter >= 60) {
			endGame = true;
			gameObject.GetComponent<Move> ().enabled = false;
			gameObject.GetComponent<BallSpawner> ().enabled = false;
		
		}
			
	}
}