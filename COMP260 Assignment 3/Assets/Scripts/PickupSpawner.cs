﻿using UnityEngine;
using System.Collections;

public class PickupSpawner : MonoBehaviour {

	public int numP = 0;
	public GameObject pickupPrefab;
	public float offSetRangemax = 1.6f;
	public float offSetRangemin = 1.5f;


	// Use this for initialization
	void Update() {
		GameObject[] spawn=GameObject.FindGameObjectsWithTag("Respawn");  

		if (spawn.Length<1 )SpawnPickup();


	}


	void SpawnPickup()
	{

		Vector2 spawnOffset = new Vector2 (Random.Range(-offSetRangemax, offSetRangemin), Random.Range(-offSetRangemax, offSetRangemin));
		Instantiate(pickupPrefab, (Vector2)transform.position + spawnOffset, Quaternion.identity);
		numP += 1;

	}



}
